#include <cstdlib>
#include <iostream>

void writeTimeSpeedMockCSV(std::ostream & os)
{
	double time = 0;
	os << "date,close" << std::endl << "0,0" << std::endl;
	for (int i = 0; i <=2000; i++)
	{
		time += 0.005;
		double speed = (std::rand() % 75000) / 10000;
		os << time << ',' << speed << std::endl;
	}
}

int main()
{
	writeTimeSpeedMockCSV(std::cout);
	return 0;
}
