#ifndef QUAD_INCLUDED
#define QUAD_INCLUDED

struct Quad
{
	Quad();
	Quad(long identifier, short x, short y, short z);
	long identifier;
	short x;
	short y;
	short z;
};

bool operator==(const Quad & lhs, const Quad & rhs);
bool operator!=(const Quad & lhs, const Quad & rhs);

#endif
