#pragma once

#include "HAL/I2C.hpp"
#include "Gyroscope.hpp"

class ITG3200Gyroscope : public Gyroscope
{
private:
	I2C& _i2c;

	short ConvertGyro(const unsigned char * buffer) const;
public:
	explicit ITG3200Gyroscope(I2C& i2c);

	virtual Raw3DSensorData ReadGyroscope() const;
};
