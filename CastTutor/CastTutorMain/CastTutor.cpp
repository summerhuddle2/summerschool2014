#include "CastTutor/SocketClient.h"
#include "CastTutor/CastRecorder.h"
#include "CastTutor/Quad.h"

#include <string>
#include <stdio.h>

int main(int argc, char *argv[])
{
	const std::string start = "start";
	const std::string stop = "stop";
	const char * socketAddress = "127.0.0.1";
	
	SocketClient socket;
	
	if(argc < 2)
	{
		printf("start or stop?");
	}
	else if(argv[1] == stop)
	{
		Quad stopQuad(0, 0, 0, 0);
		socket.OpenSenderSocket(socketAddress, 1500);
		socket.Send((char *)&stopQuad, sizeof(Quad));
	}
	else if (argv[1] == start)
	{
		socket.OpenReceiverSocket();
		CastRecorder castRecorder(&socket);
		castRecorder.CaptureCast();
	}
}
