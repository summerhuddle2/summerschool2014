#include "Quad.h"

Quad::Quad(long identifier, short x, short y, short z)
	:identifier(identifier)
	, x(x)
	, y(y)
	, z(z)
{
}

Quad::Quad()
	: identifier(0)
{
}

bool operator==(const Quad & lhs, const Quad & rhs)
{
	return lhs.identifier == rhs.identifier &&
		lhs.x == rhs.x &&
		lhs.y == rhs.y &&
		lhs.z == rhs.z;
}

bool operator!=(const Quad & lhs, const Quad & rhs)
{
	return !(lhs == rhs);
}
