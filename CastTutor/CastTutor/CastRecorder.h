#ifndef CAST_RECORDER_INCLUDED
#define CAST_RECORDER_INCLUDED

#include "ReceiverSocketInterface.h"

class CastRecorder
{
public:
	CastRecorder(ReceiverSocketInterface *socket);
	void CaptureCast();	
	ReceiverSocketInterface *_socket; 
};

#endif
