#ifndef VEC3_INCLUDED
#define VEC3_INCLUDED

struct Vec3
{
	Vec3();
	Vec3(double x, double y, double z);
	double x;
	double y;
	double z;
};

#endif

