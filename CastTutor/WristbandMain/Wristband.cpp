#include "Wristband/SocketClient.h"
#include "RaspberryPiHardware/RaspberryPiI2C.hpp"
#include "Drivers/BMA150Accelerometer.hpp"
#include "Drivers/ITG3200Gyroscope.hpp"
#include "Drivers/AK8975Compass.hpp"
#include "Wristband/WristbandQuad.h"

#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include <string>
#include <stdio.h>

int main(int /*argc*/, char* /*argv*/[])
{
	
	SocketClient socket;
	const char * socketAddress = "127.0.0.1";
	socket.Open(socketAddress, 1500);
	
	RaspberryPiI2C i2c;
	BMA150Accelerometer accelerometer(i2c);

	timeval start;

	//std::cout << std::fixed << std::setw(2) << std::setprecision(4);

	gettimeofday(&start, NULL);

	while(1)
	{
		Raw3DSensorData acceleration = accelerometer.ReadAcceleration();

		timeval current;

		gettimeofday(&current, NULL);

		const long seconds = current.tv_sec - start.tv_sec;
		const long useconds = current.tv_usec - start.tv_usec;
		const long elapsed = ((seconds) * 1000 + useconds/1000.0) + 0.5;
				
		Quad reading(elapsed, acceleration.x, acceleration.y, acceleration.z);
		socket.Send((char *)&reading, sizeof(Quad));
		
		usleep(10000);
	}
	
			return 0;
}
