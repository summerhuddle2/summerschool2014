#include <limits.h>
#include <gtest/gtest.h>
#include "CastTutor/CastRecorder.h"
#include "CastTutorMocks/MockSocketClient.h"

TEST(CastRecorder, capture_cast_exits_its_loop_when_it_receives_0quad_and_writes_data_csv_out)
{
	MockSocketClient mockSock;	
	CastRecorder castRecorder(&mockSock);
	
	mockSock.expect(Quad(1,2,3,4));
	mockSock.expect(Quad(0,2,3,4));
	
	castRecorder.CaptureCast();
	
	// assert that "data.csv" exists
	// then check its contents
	
	//ASSERT_EQ(, );
} 
