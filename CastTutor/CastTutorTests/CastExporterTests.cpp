#include "CastTutor/CastExporter.h"
#include <gtest/gtest.h>
#include <sstream>

using namespace std;

TEST(CastExport, returns_newline_terminated_time_and_speed_comma_separated)
{
	vector<Quad> rawAcceleration;
	stringstream ss;
	
	Quad a(0, 0, 0, 0);
	rawAcceleration.push_back(a);
	Quad q(5, 73, 521, 421);
	rawAcceleration.push_back(q);
	
	printTimeAndSpeed(ss , rawAcceleration);
	ASSERT_EQ("date,close\n"
			  "0,0\n"
			  "0.005,0.258115\n", ss.str());
}

TEST(CastExport, returns_newline_terminated_time_and_speed_comma_separated1)
{
	vector<Quad> rawAcceleration;
	stringstream ss;
	
	Quad q(10, 73, 521, 421);
	rawAcceleration.push_back(q);
	Quad r(15, 46, -102, 113);
	rawAcceleration.push_back(r);
	Quad s(5, 434, 1, -17);
	rawAcceleration.push_back(s);
	Quad a(0, 0, 0, 0);
	rawAcceleration.push_back(a);
	
	printTimeAndSpeed(ss, rawAcceleration);
	ASSERT_EQ("date,close\n"
			  "0,0\n"
			  "0.005,0.166381\n"
			  "0.01,0.318837\n"
			  "0.015,0.331639\n", ss.str());
}

TEST(CastExporter, sorts_a_vector_by_Quad_identifier)
{
	vector<Quad> raw;
	
	Quad q(2, 2, 2, 2);
	raw.push_back(q);
	Quad r(3, 3, 3, 3);
	raw.push_back(r);
	Quad s(1, 1, 1, 1);
	raw.push_back(s);
	
	sortVectorByQuadIdentifier(raw);
	ASSERT_EQ(s, raw[0]); 
	ASSERT_EQ(q, raw[1]);
	ASSERT_EQ(r, raw[2]);
}

